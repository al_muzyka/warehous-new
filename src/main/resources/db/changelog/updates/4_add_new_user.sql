-- liquibase formatted sql
-- changeset almu:4

INSERT INTO users (username, password, enabled) VALUES ('user', '{bcrypt}$2a$10$YcpAra0MrqkTLo065dGvRefZLhwoG7Pey7C0EOPMdIikfmf0Rqkhi', 1);
INSERT INTO authorities (username, authority) VALUES ('user', 'ROLE_USER');