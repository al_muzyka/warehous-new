package com.almu.warehous;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@SpringBootApplication
public class WarehousApplication {

	@Bean
	public DataSource dataSource() {
		return DataSourceBuilder
				.create()
				.url("jdbc:mysql://mysql:3306/warehous")
				.username("root")
				.password("root")
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(WarehousApplication.class, args);
	}

}
